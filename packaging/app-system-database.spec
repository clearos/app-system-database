
Name: app-system-database
Epoch: 1
Version: 2.3.4
Release: 1%{dist}
Summary: System Database
License: GPLv3
Group: ClearOS/Apps
Source: %{name}-%{version}.tar.gz
Buildarch: noarch
Requires: %{name}-core = 1:%{version}-%{release}
Requires: app-base
Requires: phpMyAdmin >= 4.1.13

%description
The System Database app provides a built-in database that can be used by other apps.

%package core
Summary: System Database - Core
License: LGPLv3
Group: ClearOS/Libraries
Requires: app-base-core
Requires: app-storage-core >= 1:1.4.7
Requires: system-mariadb-server >= 1:5.5.52-2
Requires: webconfig-php-mysql

%description core
The System Database app provides a built-in database that can be used by other apps.

This package provides the core API and libraries.

%prep
%setup -q
%build

%install
mkdir -p -m 755 %{buildroot}/usr/clearos/apps/system_database
cp -r * %{buildroot}/usr/clearos/apps/system_database/

install -d -m 0755 %{buildroot}/var/clearos/events/system_database
install -d -m 0755 %{buildroot}/var/clearos/system_database
install -d -m 0755 %{buildroot}/var/clearos/system_database/state
install -D -m 0755 packaging/rmysql %{buildroot}/usr/sbin/rmysql
install -D -m 0644 packaging/system-mariadb.php %{buildroot}/var/clearos/base/daemon/system-mariadb.php
install -D -m 0644 packaging/system_database.php %{buildroot}/etc/phpMyAdmin/system_database.php
install -D -m 0644 packaging/system_database_default.conf %{buildroot}/etc/clearos/storage.d/system_database_default.conf
install -D -m 0755 packaging/system_database_event %{buildroot}/var/clearos/events/system_database/system_database
install -D -m 0755 packaging/systemdb %{buildroot}/usr/sbin/systemdb

%post
logger -p local6.notice -t installer 'app-system-database - installing'

%post core
logger -p local6.notice -t installer 'app-system-database-core - installing'

if [ $1 -eq 1 ]; then
    [ -x /usr/clearos/apps/system_database/deploy/install ] && /usr/clearos/apps/system_database/deploy/install
fi

[ -x /usr/clearos/apps/system_database/deploy/upgrade ] && /usr/clearos/apps/system_database/deploy/upgrade

exit 0

%preun
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer 'app-system-database - uninstalling'
fi

%preun core
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer 'app-system-database-core - uninstalling'
    [ -x /usr/clearos/apps/system_database/deploy/uninstall ] && /usr/clearos/apps/system_database/deploy/uninstall
fi

exit 0

%files
%defattr(-,root,root)
/usr/clearos/apps/system_database/controllers

%files core
%defattr(-,root,root)
%exclude /usr/clearos/apps/system_database/packaging
%exclude /usr/clearos/apps/system_database/unify.json
%dir /usr/clearos/apps/system_database
%dir /var/clearos/events/system_database
%dir /var/clearos/system_database
%dir /var/clearos/system_database/state
/usr/clearos/apps/system_database/deploy
/usr/clearos/apps/system_database/language
/usr/clearos/apps/system_database/libraries
/usr/sbin/rmysql
/var/clearos/base/daemon/system-mariadb.php
/etc/phpMyAdmin/system_database.php
/etc/clearos/storage.d/system_database_default.conf
/var/clearos/events/system_database/system_database
/usr/sbin/systemdb
