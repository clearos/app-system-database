<?php

/////////////////////////////////////////////////////////////////////////////
// General information
/////////////////////////////////////////////////////////////////////////////

$app['basename'] = 'system_database';
$app['version'] = '2.3.4';
$app['release'] = '1';
$app['vendor'] = 'ClearFoundation';
$app['packager'] = 'ClearFoundation';
$app['license'] = 'GPLv3';
$app['license_core'] = 'LGPLv3';
$app['description'] = lang('system_database_app_description');

/////////////////////////////////////////////////////////////////////////////
// App name and categories
/////////////////////////////////////////////////////////////////////////////

$app['name'] = lang('system_database_app_name');
$app['category'] = lang('base_category_system');
$app['subcategory'] = lang('base_subcategory_settings');
$app['menu_enabled'] = FALSE;

/////////////////////////////////////////////////////////////////////////////
// Packaging
/////////////////////////////////////////////////////////////////////////////

$app['core_requires'] = array(
    'app-storage-core >= 1:1.4.7',
    'system-mariadb-server >= 1:5.5.52-2',
    'webconfig-php-mysql',
);

$app['requires'] = array(
    'phpMyAdmin >= 4.1.13',
);

$app['core_file_manifest'] = array(
    'system_database_default.conf' => array ('target' => '/etc/clearos/storage.d/system_database_default.conf'),
    'system_database.php' => array ('target' => '/etc/phpMyAdmin/system_database.php'),
    'system-mariadb.php' => array('target' => '/var/clearos/base/daemon/system-mariadb.php'),
    'systemdb' => array(
        'target' => '/usr/sbin/systemdb',
        'mode' => '0755'
    ),
    'rmysql' => array(
        'target' => '/usr/sbin/rmysql',
        'mode' => '0755'
    ),
    'system_database_event'=> array(
        'target' => '/var/clearos/events/system_database/system_database',
        'mode' => '0755'
    ),
);

$app['core_directory_manifest'] = array(
    '/var/clearos/events/system_database' => array(),
    '/var/clearos/system_database' => array(),
    '/var/clearos/system_database/state' => array(),
);
