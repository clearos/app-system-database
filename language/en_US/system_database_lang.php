<?php

$lang['system_database_app_name'] = 'System Database';
$lang['system_database_app_description'] = 'The System Database app provides a built-in database that can be used by other apps.';
$lang['system_database_system_database_storage'] = 'System Database Storage';
$lang['system_database_database_management'] = 'System Database Management';
